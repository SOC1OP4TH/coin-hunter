This is a simple 3D platformer game made with Unity. The player controls a ball that can jump and move around the world. The goal of the game is to collect coins and reach the end of the level.

## Controls

* W, A, S, D to move
* Space to jump

## Gameplay

The player must collect coins and avoid obstacles in order to reach the end of the level. If the player falls off the world or hits an obstacle, they will respawn at the beginning of the level.

## Heals

The player has 3 heals. If the player falls off the world, they will lose a heal. If the player loses all of their heals, the game will end.

## Triggers

* Coin triggers will give the player a coin.
* TP triggers will teleport the player to a different location.
* To win the game, the player must reach the end of the level with at least one heal remaining.

## Tips

* Use the jump button to jump over obstacles.
* Be careful not to fall off the world.
* Collect coins to increase your score.
* Use TP triggers to teleport to different locations.
Enjoy!
