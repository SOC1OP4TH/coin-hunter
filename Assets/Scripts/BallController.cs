using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    bool isJumping =false;
    Rigidbody rb;

    [SerializeField]
    bool isGround = true;
    float speed =150;
    // Start is called before the first frame update
    void Start()
    {
        rb= GetComponent<Rigidbody>();
    }
    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
    }
    void Move()
    {
        float xInput = Input.GetAxis("Horizontal");
        float zInput =  Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(xInput,0,zInput);
        rb.AddForce(direction*speed*Time.deltaTime);
    }
    void Jump()
    {
        if(Input.GetKeyDown(KeyCode.Space) && !isJumping )
        {
            rb.AddForce(Vector3.up*speed*2*Time.deltaTime,ForceMode.Impulse);
            isJumping = true;
        }
    }    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }


    
}
