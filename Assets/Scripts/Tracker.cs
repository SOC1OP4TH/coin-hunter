using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tracker : MonoBehaviour
{
    // Start is called before the first frame update'
    public Transform target;
    void Update()
    {
        Track();
    }
    void Track()
    {
        transform.LookAt(target);
    }
}
