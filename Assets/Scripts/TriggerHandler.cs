using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHandler : MonoBehaviour
{
  int coins = 0;
  Vector3 pos = new Vector3(-42.59f,8f,-38.5f);
  void OnTriggerEnter(Collider other)
  {
       if(other.gameObject.tag=="Coin")
       {
        coins++;
        Destroy(other.transform.parent.gameObject);
        Debug.Log($"U have {coins} coins");
       } 

       if(other.gameObject.tag == "TP")
       {
        Debug.Log("TP");
        transform.position = pos;
       }
  }  
}
